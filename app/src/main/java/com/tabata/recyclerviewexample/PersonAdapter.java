package com.tabata.recyclerviewexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tabata.recyclerviewexample.model.Person;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wenceslaus on 06.05.17.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder>{


    interface PersonClickListener {
        void onClick(Person person);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<Person> list;
    private PersonClickListener personClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            personClickListener.onClick(list.get(position));
        }
    };

    public PersonAdapter(Context context, List<Person> list, PersonClickListener personClickListener) {
        this.context = context;
        this.list = list;
        this.personClickListener = personClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person person = list.get(position);
        holder.name.setText(person.getName());
        holder.position.setText(person.getPosition());
        holder.country.setText(person.getCountry());
        holder.rectangle.setBackgroundColor(person.getColor());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.name)
        TextView name;
        TextView position;
        TextView country;
        View rectangle;

        ItemClickListener itemClickListener;

        public ViewHolder(View item, final ItemClickListener itemClickListener) {
            super(item);
            this.itemClickListener = itemClickListener;
//            ButterKnife.bind(this, item);
            name = (TextView) item.findViewById(R.id.name);
            position = (TextView) item.findViewById(R.id.position);
            country = (TextView) item.findViewById(R.id.country);
            rectangle = item.findViewById(R.id.rectangle);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }

//        @OnClick(R.id.root)
//        void onClick() {
//            itemClickListener.onClick(getAdapterPosition());
//        }

    }

}
