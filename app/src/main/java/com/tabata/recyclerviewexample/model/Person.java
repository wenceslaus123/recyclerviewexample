package com.tabata.recyclerviewexample.model;

import android.support.annotation.ColorInt;

import java.io.Serializable;

/**
 * Created by wenceslaus on 06.05.17.
 */

public class Person implements Serializable{
    private String name;
    private String position;
    private String country;

    @ColorInt
    private int color;

    public Person(String name, String position, String country, int color) {
        this.name = name;
        this.position = position;
        this.country = country;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getCountry() {
        return country;
    }

    public int getColor() {
        return color;
    }
}
