package com.tabata.recyclerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.tabata.recyclerviewexample.model.Person;

import static com.tabata.recyclerviewexample.MainActivity.KEY;

public class PersonActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        textView = (TextView) findViewById(R.id.name);

        if (getIntent() != null) {
            Person person = (Person) getIntent().getSerializableExtra(KEY);
            textView.setText(person.getName());
        }
    }
}
